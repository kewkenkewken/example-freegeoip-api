var mainModule = angular.module('myApp', []);

mainModule.controller('checkCtrl', function($scope, $http) {

    $scope.getData = function(ip) {
        $http({
            method: 'GET',
            url: 'http://www.freegeoip.net/json/' + ip
        }).
        success(function(data) {
            $scope.ip = data.ip ;
            $scope.country_name = data.country_name ;
            $scope.region_name = data.region_name ;
            $scope.city = data.city ;
            $scope.zip_code = data.zip_code ;
            $scope.latitude = data.latitude ;
            $scope.longitude = data.longitude ;
            $scope.metro_code = data.metro_code ;
            $scope.time_zone = data.time_zone ;
        }).
        error(function(data) {

        });
    }

});
